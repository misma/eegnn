#!/usr/bin/python
#Log data
ID='_13'
optimizer_='Adam'
learning_rate=0.001
No_of_epochs=10
batch=20
validation_q=0.5
loss_type='categorical_crossentropy'
metrics_type='categorical_accuracy'
No_of_samples=20

file = open('logs/'+ID+'.log','w') 
file.write('ID='+ID)
file.write('/n') 
file.write('optimizer='+optimizer_) 
file.write('/n') 
file.write('learning_rate='+str(learning_rate)) 
file.write('/n') 
file.write('No_of_epochs='+str(No_of_epochs))
file.write('/n') 
file.write('batch_size='+str(batch))
file.write('/n') 
file.write('validation_split='+str(validation_q))
file.write('/n') 
file.write('loss='+loss_type)
file.write('/n') 
file.write('metrics='+metrics_type) 
file.write('/n') 
file.write('No_of_samples='+str(No_of_samples))
file.write('/n') 
#file.write('Dilated conv=true') 
#file.write('bidirectional lstm=false illetve lstm return seq=False')

file.close() 



import importlib
import numpy as np
from sklearn import preprocessing



'''

x_data=np.load('../adatfeldolg/eeg2.npy')
y_data=np.load('../adatfeldolg/imgresult.npy')[:,1]

max=len(x_data[0])
for i in range(1,len(x_data)):
	if len(x_data[i]) > max:
		max = len(x_data[i])

for i in range(0,len(x_data)):
	x_data[i]=np.pad(x_data[i],((0,max-len(x_data[i]))),mode='constant', constant_values=0)
temp=x_data[0]
for i in range(1,len(x_data)):
	temp = np.row_stack((temp,x_data[i]))
x_data=temp
x_data=np.array(x_data).astype('float32')
#x_data=np.asarray(x_data)
x_data=x_data.reshape((len(x_data),max,1))
print x_data
print type(x_data)
print max
print x_data.shape


#print str(len(x_data))+' '+str(len(y_data))
#print y_data

print 'dimenzio:' + str(x_data.ndim)

#x_data=x_data.reshape(x_data.shape[0],1,)

#np.savetxt("eeg_data.csv", data, delimiter=",")
#np.savetxt("y_data.csv", y_data, delimiter=",")

np.save('eegdata',x_data)
'''
x_data=np.load('eegdata.npy')
y_data=np.load('imgresult.npy')[:,1]

print x_data.shape


print 'normalization/n'
#std_scale = preprocessing.StandardScaler().fit(x_data)
#x_data = std_scale.transform(x_data)
'''
for i in range (0,len(x_data)):
	x_data[i] = preprocessing.normalize(x_data[i], norm='l2')
print x_data[0]
'''
from keras.models import Sequential
from keras import layers
from keras.layers import LSTM
from keras.layers import TimeDistributed
from keras.utils import plot_model
from keras.utils.np_utils import to_categorical

categorical_labels = to_categorical(y_data, num_classes=3)

print categorical_labels

indices = np.random.randint(0,x_data.shape[0],No_of_samples)
x_data_random_sample = x_data[indices]
categorical_labels_random_sample=categorical_labels[indices]

print (len(x_data_random_sample[0]),1)
print x_data_random_sample.shape

x_data_random_sample=np.reshape(x_data_random_sample,[20, len(x_data_random_sample[0]),1])

model=Sequential() #egymas utan jovo layerek
#model.add(layers.Conv1D(filters=32,kernel_size=9,strides=2,dilation_rate=2,activation='relu',input_shape=(len(x_data_random_sample[0]),1)))
#tobb feATURET szedjunk ki
#model.add(layers.MaxPooling1D(3))
#model.add(layers.Conv1D(filters=64,kernel_size=32,strides=9,activation='relu')) #idoinvarians itt is tobb featuret 
#kiszedni dilated conv or atrous conv
#model.add(layers.MaxPooling1D(3))
#model.add(layers.Conv1D(128,9,activation='relu'))
#model.add(layers.GlobalMaxPooling1D())
#model.add(layers.LSTM(64,return_sequences=False))
#model.add(layers.LSTM(32)) #cella belso allapota megvaltozik ugyanarra a bemenetre es mast ad ki - idobeliseget visz bele, modellek sec 
#2 sec kimenet transformalt adat, de nekunk a final state kell. final statet kapom meg? bidirectional lstm

model.add((layers.TimeDistributed(layers.Dense(3,activation='softmax',input_shape=(len(x_data_random_sample[0]),1)))))

#ADAM jobb lenne -> van momentuma nagyobb valseggel kerul ki a lok minbol

#from keras.optimizers _import_(optimizer_)
#optimizer_imported=__import__('keras.optimizers.'+optimizer_)

optimizers_imported=importlib.import_module("keras.optimizers")
opt =getattr(optimizers_imported,optimizer_)(lr=learning_rate)

model.compile(optimizer=opt,loss=loss_type,metrics=[metrics_type])
plot_model(model,show_shapes='True',to_file='models/model'+ID+'.png')

print categorical_labels_random_sample.shape

categorical_labels_random_sample=np.reshape(categorical_labels_random_sample,[20,1,3])
print categorical_labels_random_sample.shape

history=model.fit(x_data_random_sample,categorical_labels_random_sample,epochs=No_of_epochs,batch_size=batch,validation_split=validation_q)

import matplotlib.pyplot as plt

loss=history.history['loss']
val_loss=history.history['val_loss']

epochs=range(1,len(loss)+1)

plt.figure(1)
plt.plot(epochs,loss,'bo',label='Training loss')
plt.plot(epochs,val_loss,'b',label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.savefig('figures/loss'+ID+'.png')

plt.clf

plt.figure(2)

acc=history.history['acc']
val_acc=history.history['val_acc']

plt.plot(epochs,acc,'bo',label='Training acc')
plt.plot(epochs,val_acc,'b',label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()
plt.savefig('figures/acc'+ID+'.png')



plt.figure(3)
plt.plot(epochs,loss,'bo',label='Training loss')
plt.plot(epochs,val_loss,'b',label='Validation loss')
plt.title('Training and validation loss and accuracy')
plt.xlabel('Epochs')
plt.ylabel('Loss and accuracy')
plt.savefig('figures/loss'+ID+'.png')

acc=history.history['acc']
val_acc=history.history['val_acc']

plt.plot(epochs,acc,'ro',label='Training acc')
plt.plot(epochs,val_acc,'r',label='Validation acc')
plt.title('Training and validation accuracy')
plt.legend()
plt.savefig('figures/egyben'+ID+'.png')

#10-10 mintan kiprobalni train acc es train losst - ha ezen mukodik lehet feljebb menni 100ig.
#validation csucsan kene megallitani. de a training meg megy feljebb. adatnormalas..
